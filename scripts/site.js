const alturaIni = 180; /*debe ser el mismo que headerContent[height] */
const alturaScr = 60
var alturaCabecera = alturaIni; 
var inicio = true;
var topLimite = 0;
var sliderInPage = false;

var imageDataIndex = [ 
	'imagenes/mediCardh.jpg', 'imagenes/yogaCardh.jpg', 'imagenes/tachCardh.jpg', 'imagenes/educCardh.jpg', 'imagenes/capaCardh.jpg', 
	'docentes/docente1f.jpg', 'docentes/docente2f.jpg', 'docentes/docente3f.jpg', 'docentes/docente4f.jpg', 'docentes/docente5f.jpg', 'docentes/docente6f.jpg', 'docentes/docente7f.jpg', 'docentes/docente8f.jpg', 'docentes/docente9f.jpg'
];

function slideSection(section) {
	$("body, html").animate({ scrollTop: $('#' + section).offset().top - alturaScr }, 1000);
};

function headerSize(headerHeight, imageHeight, menuTop) {
	$("#headerContentMain").animate({height:headerHeight + "px"}, 1000);
	$(".menuContainer").animate({top: menuTop + "px"}, 1000);
	$('.headerLogo img').animate({height:imageHeight + "px"}, 1000);
}

function InsertHighlightText(paragraphNumber) {
	var paragraphElment = '.bodyText p:nth-child(' + paragraphNumber + ')';
	var cutParagraph = $(paragraphElment).html();
	$(paragraphElment)
		.html(cutParagraph
				.substring(1, cutParagraph.length / 2)
				.concat("<span class='highlightText'>")
				.concat($('#highlightTextReference').text())
				.concat("</span>")
				.concat(cutParagraph.substring(cutParagraph.length / 2, cutParagraph.length )
			)
		);
	$('#highlightTextReference').remove();
}

function preloadImage(data)
{
    var total = data.length;
    var imgArray = new Array();
    for(var i=0; i<total; i++)
    {
        var tmpImage = new Image();
        tmpImage.src = data[i];
        imgArray.push(tmpImage);
    }
}

$(function () {
	/* Initial Settings */
	if(sliderInPage)
		IDH_slider_init();

	$(".headerContentHeight").css("height", alturaIni + "px");	
	$('#backTopButton').hide();
	$('#backgroundLayer').hide();
	$('#teacherPopUp').hide();
	$('#weArePopUp').hide();
	$('#indexForm').slideUp();

	topLimite = $('.menuLimitSection').offset().top;

	var actualSection = $(location).attr('href').split("#")[1];
	if (actualSection) {
		inicio = true; /* por verificar si es necesario */
		slideSection(actualSection);
	}

	$('#hamburger').click(function() {

		$('#hamburger img')
			.slideUp("fast")
			.attr("src", $('.menuContainer').is(':visible') ? 'imagenes/hamburger.png' : 'imagenes/hamburgerX.png')
			.slideDown("fast");
		
		$('.menuContainer').slideToggle('medium', function() {
			if ($(this).is(':visible'))
				$(this).css('display','flex');
			else
				$(this).removeAttr('style');
		});
	});

	$('.menuOption').click(function() {
		switch($(this).data('type')) {
			case 'interno':
				slideSection($(this).data('url'));	
				break;
			case 'externo':
				$(location).attr("href", $(this).data('url'));
				break;
			default:
				console.log('No hay tipo de link.')
				break;
		}
	});
	
	$('#backTopButton').click(function() {
		slideSection('headerContentFake');
	});
	
	$('.paginaTienda .card, .horarios .card').click(function() {
		alert('Añadido al Carrito');
	});
	
	$(window).scroll(function () {
		if ($(window).scrollTop() > (topLimite - alturaIni)) {
			if (inicio) {
				inicio = false;
				alturaCabecera = alturaScr;
				headerSize(alturaCabecera, 45, 40);
				$('#backTopButton').slideDown('slow');
			}
		} else {
			if (!inicio) {
				inicio = true;
				alturaCabecera = alturaIni;
				headerSize(alturaCabecera, 100, 100);
				$('#backTopButton').slideUp('slow');
			}
		}
	});
		
	$('.card.teacher').click(function() {
		$('#backgroundLayer').show();
		$('#teacherPopUp .teacherPopUpBody').hide();
		$('#teacherPopUp .teacherPopUpBody.pu-' + $(this).attr('data-teacherid')).show();
		$('#teacherPopUp').slideDown('slow');
		$('.homePage').css("overflow", "hidden");
	});
	
	$('#teacherPopUp .close').click(function() {
		$('#backgroundLayer').hide();
		$(".teacherPopUpBody, .teacherCV").scrollTop(0);
		$('#teacherPopUp').slideUp('fast');
		$('.homePage').css("overflow", "auto");
	});
	
	/* Links de Video */
	$('.nosotros .linkTexto').click(function() {
		$('#backgroundLayer').show();
		$('#weArePopUp').slideDown('slow');
		$('.weArePopUpBody div').hide();
		$('.weArePopUpBody div.section-' + this.id).show();
		$('.homePage').css("overflow", "hidden");
		//$('.videoPopUpBody iframe').attr("src", $(this).attr('data-url'));
	});
	
	$('#weArePopUp .close').click(function() {
		$('#backgroundLayer').hide();
		$('.weArePopUpBody').scrollTop(0);
		$('#weArePopUp').slideUp('fast');
		$('.homePage').css("overflow", "auto");
		//$('.videoPopUpBody iframe').attr("src", $('.videoPopUpBody iframe').attr("src"));
		//$('.videoPopUpBody iframe').attr("src", "");
	});
	
	/* Formulario */
	$('#formSection').click(function() {
		$('#indexForm').slideToggle(function() {
			$('#formSection')
				.removeClass('close')
				.removeClass('open')
				.addClass($('#indexForm').is(":visible") ? 'open' : "close");
				
				if ($('#indexForm').is(":visible"))
					$("body, html").animate({ scrollTop: $('#formSection').offset().top }, 1000);
		});
	});
});